import { indexSpryData } from './actions';

class SpryIndex {
  constructor(props) {
    this.store = props.store;
    this.store.subscribe(() => this.handleStoreStateChange());
    this.sortedSprints = [];
    this.sortedTasks = [];
    this.contributors = null;
    this.projects = null;
  }

  handleStoreStateChange() {
    if (this.store.getState().client.dataReceived && !this.store.getState().client.dataIndexed) {
      // Update the indexes
      this.updateIndex();
      this.store.dispatch(indexSpryData());
    }
  }

  getSprint(id) {
    if (this.store.getState().spry.sprints && this.store.getState().spry.sprints[id]) {
      return this.store.getState().spry.sprints[id];
    }
    return null;
  }
  
  getContributor(alias) {
    if (this.store.getState().spry.contributors && this.store.getState().spry.contributors[alias]) {
      return this.store.getState().spry.contributors[alias];
    }
    return null;
  }

  hasSprints() {
    return this.sortedSprints.length > 0;
  }

  hasContributors() {
    return this.contributors !== null;
  }

  hasProjects() {
    return this.projects !== null;
  }

  isEmpty() {
    return !this.hasSprints() || !this.hasContributors(); //|| !this.hasProjects();
  }

  updateIndex() {
    let spry = this.store.getState().spry;

    if (!spry) return;

    if (spry.sprints) {
      let sprints = Object.values(spry.sprints);
      sprints.sort((a, b) => b.startDate - a.startDate);
      this.sortedSprints = sprints;
    }
    
    if (spry.contributors) {
      let contributors = {...spry.contributors};
      this.contributors = contributors;
    }

    if (spry.tasks) {
      let tasks = Object.keys(spry.tasks).sort().reduce((obj, key) => {
        obj[key] = spry.tasks[key];
        return obj;
      }, {});
      this.sortedTasks = tasks;
    }
  }
}

export default SpryIndex;