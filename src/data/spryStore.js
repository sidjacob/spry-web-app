// import fetch from 'isomorphic-fetch';
import { Promise } from 'es6-promise';


class SpryStore {}

class S3Store extends SpryStore {

  fetchSpryData() {

    const promise = new Promise((resolve, reject) => {
      setTimeout(function() { resolve(getEmptyData()) }, 2000);
    });

    return promise;

  }
}

function getSampleData() {
  return {
    sprints: {
      'v1.7': {
        id: 'v1.7',
        name: 'v1.7',
        startDate: new Date('2017-08-01')
      },
      'v1.8': {
        id: 'v1.8',
        name: 'v1.8',
        startDate: new Date('2017-09-01')
      }
    },
    contributors: {
      'sidjacob': {
        alias: 'sidjacob',
        name: 'Sid Jacob',
        role: 'SDM'
      }
    },
    tasks: {
      '1234': {
        title: 'Task Title',
        description: 'task description',
        assignee: 'sidjacob',
        timeEstimate: 1234,
        dependencies: ['1233', '1232'],
        priority: null,
        project: 'awesomeProject',
        sprint: 'sprint1'
      }
    }
  }
}

function getEmptyData() {
  return {};
}

export default new S3Store();
