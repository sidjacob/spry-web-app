import React from 'react';
import { Modal, Button, Icon, Container } from 'semantic-ui-react';

/**
 * This is abstract class that supports modals used to create and update
 * specific records.
 * 
 * The subclass must implement the following methods:
 * initializePropertiesForNewInstance() - Called during a save to Initialize properties for a new instance.
 * validate() - Validate the instance. Returns a validation result.
 * convertDataTypes() - Converts the strings on an instance to the proper data type.
 * dispatchCreateAction() - Dispatch the Redux event to create the instance.
 * getInitialState() - Returns the initial state. Called when initializing and after cancelling or a succesful save.
 * getTitle() - Returns the title to display for the form.
 */
class CreateUpdateView extends React.Component {
    
    constructor(props) {
      super(props);
      this.state = {...this.getDefaultState(), ...this.getInitialState()};
      this.open = this.open.bind(this);
      this.save = this.save.bind(this);
      this.cancel = this.cancel.bind(this);
      this.useModal = !(this.props.modal !== undefined && this.props.modal === 'false');
    }
    
    getDefaultState() {
        return {isCreate: true, open: false, validationResult: {hasErrors: false, fields: {}}};
    }
    
    open() {
      this.setState({...this.state, open: true});
    }
    
    save() {
        if (this.state.isCreate) {
           this.initializePropertiesForNewInstance();
        }
        
        let validationResult = this.validate();
        if (validationResult.hasErrors) {
          this.setState({...this.state, validationResult: validationResult});
        } else {
            this.convertDataTypes();
            this.dispatchCreateAction();
            this.reset();
        }
    }
    
    cancel() {
      this.reset();
    }
    
    reset() {
        this.setState({...this.getDefaultState(), ...this.getInitialState()});
    }
    
    renderActions() {
      if (this.useModal) {
        return <div>
               <Button onClick={this.save} color='green' basic>
                 <Icon name='checkmark' /> Save
               </Button>
               <Button onClick={this.cancel} color='grey' basic>
                 <Icon name='remove' /> Cancel
               </Button>
             </div>;  
      } else {
         return <div>
               <Button onClick={this.cancel} color='grey' basic floated='right'>
                 <Icon name='remove' /> Cancel
               </Button>
               <Button onClick={this.save} color='green' basic floated='right'>
                 <Icon name='checkmark' /> Save
               </Button>
             </div>;
      }
    }
    
    renderModal() {
      return <Modal open={this.state.open}>
               <Modal.Header>{this.getTitle()}</Modal.Header>
               <Modal.Content>{this.renderForm()}</Modal.Content>
               <Modal.Actions>{this.renderActions()}</Modal.Actions>
             </Modal>;
    }
    
    renderInline() {
      return <div>
              {this.renderForm()}
              <Container className='create-update-view-inline-actions'>
                {this.renderActions()}
              </Container>
             </div>;
    }
    
    render() {
      if (this.useModal) {
        return this.renderModal();
      } else {
        return this.renderInline();
      }
    }
}

export default CreateUpdateView;
