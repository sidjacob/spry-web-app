import React from 'react';
import { Form, Dropdown } from 'semantic-ui-react';
import { createContributor } from '../../data/actions';
import Validator from '../../data/validator';
import CreateUpdateView from '../base/CreateUpdateView';
import { getContributorRoles, getContributorSkills } from '../../config/spryConfig';

class ContributorCreateUpdateView extends CreateUpdateView {

  constructor(props) {
    super(props);
    this.onInputChange = this.onInputChange.bind(this);
  }

  onInputChange(event, data) {

    if (data.name === 'skills') {
      this.state.contributor[data.name] = data.value;
    }
    else {
      this.state.contributor[data.name] = data.value.trim();
    }
    this.setState(this.state);
  }

  getInitialState() {
    return { contributor: {} };
  }

  initializePropertiesForNewInstance() {
    // No properties to initialize
  }

  validate() {
    let validator = new Validator();

    let getContributor = alias => this.props.spryIndex.getContributor(alias);

    return validator
      .validateAlias('alias', this.state.contributor.alias)
      .validateUniqueness('alias', this.state.contributor.alias, getContributor)
      .validateName('name', this.state.contributor.name)
      .validateNotBlank('role', this.state.contributor.role)
      .getResult();
  }

  convertDataTypes() {
    // No datatypes to convert
  }

  dispatchCreateAction() {
    this.props.store.dispatch(createContributor(this.state.contributor));
  }
  
  getTitle() {
    return this.state.isCreate ? 'Add contributor' : 'Edit contributor'
  }

  renderForm() {

    const roles = getContributorRoles();
    roles.sort((a, b) => a.name.localeCompare(b.name));

    const rolesRadioButtons = roles.map(role => {
      return <Form.Radio key={role.id} checked={role.id === this.state.contributor.role}
                name='role' label={role.name} value={role.id} onChange={this.onInputChange} />;
    });

    const skills = getContributorSkills();
    skills.sort((a, b) => a.name.localeCompare(b.name));

    const skillOptions = skills.map(skill => {
      return { key: skill.id, value: skill.id, text: skill.name };
    });

    return (
      <Form>
        <Form.Group widths='equal'>
          <Form.Field>
            <label>Alias *</label>
            <Form.Input error={this.state.validationResult.fields.alias} name='alias'
              onChange={this.onInputChange} placeholder='Minimum 3 characters' />
          </Form.Field>
          <Form.Field>
            <label>Name *</label>
            <Form.Input error={this.state.validationResult.fields.name} name='name'
              onChange={this.onInputChange} placeholder='Minimum 2 characters' />
          </Form.Field>
        </Form.Group>
        <Form.Group inline>
          <label style={this.state.validationResult.fields.role ? {color: 'red'} : {}}>Role *</label>
          {rolesRadioButtons}
        </Form.Group>
        <Form.Field>
        <Dropdown name="skills" placeholder='Skills' fluid multiple selection options={skillOptions}
            onChange={this.onInputChange} />
        </Form.Field>
      </Form>
    );
  }
}

export default ContributorCreateUpdateView;
