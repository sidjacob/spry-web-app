import React from 'react';
import { Button, Container, Header, Table, Step } from 'semantic-ui-react';
import _ from 'lodash';

import TaskCreateUpdateView from './TaskCreateUpdateView';

class TasksView extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      column: null,
      data: this.props.spryIndex.sortedTasks,
      direction: null,
    };

    this.props.store.subscribe(() => this.handleStoreStateChange());
  }

  handleStoreStateChange() {
    this.setState({ ...this.state, data: this.props.spryIndex.sortedTasks });
  }

  handleSort = clickedColumn => () => {
    const { column, data, direction } = this.state;

    if (column !== clickedColumn) {
      this.setState({
        column: clickedColumn,
        data: _.sortBy(data, [clickedColumn]),
        direction: 'ascending',
      });

      return
    }

    this.setState({
      data: data.reverse(),
      direction: direction === 'ascending' ? 'descending' : 'ascending',
    })
  };

  render() {
    const { column, data, direction } = this.state;

    return (
      <div>
        <Container>
          <Header>Tasks</Header>
          <Table sortable celled striped>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell sorted={column === 'name' ? direction : null} onClick={this.handleSort('id')}>
                  Id
                </Table.HeaderCell>
                <Table.HeaderCell sorted={column === 'age' ? direction : null} onClick={this.handleSort('title')}>
                  Title
                </Table.HeaderCell>
                <Table.HeaderCell sorted={column === 'gender' ? direction : null} onClick={this.handleSort('assignee')}>
                  Assignee
                </Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {_.map(data, ({ title, assignee }, key) => (
                <Table.Row key={key}>
                  <Table.Cell>{key}</Table.Cell>
                  <Table.Cell>{title}</Table.Cell>
                  <Table.Cell>{assignee}</Table.Cell>
                </Table.Row>
              ))}
            </Table.Body>
          </Table>

          <Button icon='plus' content='New task' color='blue' onClick={() => {this.createView.open();}} />

          <TaskCreateUpdateView store={this.props.store} spryIndex={this.props.spryIndex} ref={el => { this.createView = el }} />
        </Container>

      </div>
    );
  }
}

export default TasksView;
