import React from 'react';
import { Form, Dropdown } from 'semantic-ui-react';
import { createTask } from '../../data/actions';
import Validator from '../../data/validator';
import CreateUpdateView from '../base/CreateUpdateView';

class TaskCreateUpdateView extends CreateUpdateView {

  constructor(props) {
    super(props);
    this.onInputChange = this.onInputChange.bind(this);
  }

  onInputChange(event, data) {
    console.log('event:', event);
    this.state.task[data.name] = data.value.trim();
    this.setState(this.state);
  }

  getInitialState() {
    return { task: {} };
  }

  initializePropertiesForNewInstance() {
    // No properties to initialize
  }

  validate() {
    let validator = new Validator();

    return validator
      .validateAlias('title', this.state.task.title)
      .validateNotBlank('description', this.state.task.description)
      .validateNotBlank('assignee', this.state.task.assignee)
      .getResult();
  }

  convertDataTypes() {
    // No datatypes to convert
  }

  dispatchCreateAction() {
    this.props.store.dispatch(createTask(this.state.task));
  }
  
  getTitle() {
    return this.state.isCreate ? 'Add task' : 'Edit task'
  }

  renderForm() {

    const assigneeOptions = Object.values(this.props.spryIndex.contributors).map((contributor) => {
      return {key: contributor.alias, value: contributor.alias, text: contributor.name};
    }, {});

    // Fill these in from the index when they exist.
    const dependenciesOptions = [];
    const projectOptions = [];
    const sprintOptions = [];

    const { title, assignee, description } = this.state.task;

    /*
        title: 'Task Title',
        description: 'task description',
        assignee: 'sidjacob',
        timeEstimate: 1234,
        dependencies: ['1233', '1232'],
        priority: null,
        project: 'awesomeProject',
        sprint: 'sprint1'
     */

    return (
      <Form>
        <Form.Field required>
          <label>Title</label>
          <Form.Input error={this.state.validationResult.fields.title} name='title' value={title} onChange={this.onInputChange} />
        </Form.Field>
        <Form.Field required>
          <label>Description</label>
          <Form.TextArea error={this.state.validationResult.fields.name} name='description' value={description} onChange={this.onInputChange} />
        </Form.Field>
        <Form.Field required>
          <label>Assignee</label>
          <Dropdown error={this.state.validationResult.fields.assignee} name='assignee' value={assignee}
                    fluid search selection options={assigneeOptions} onChange={this.onInputChange} />
        </Form.Field>
        <Form.Field>
          <label>Dependencies</label>
          <Dropdown name="dependencies" fluid search multiple selection options={dependenciesOptions} onChange={this.onInputChange} />
        </Form.Field>
        <Form.Field>
          <label>Priority</label>
          <Form.Input error={this.state.validationResult.fields.priority} name='priority' onChange={this.onInputChange} />
        </Form.Field>
        <Form.Field>
          <label>Project</label>
          <Dropdown name="project" fluid search multiple selection options={projectOptions} onChange={this.onInputChange} />
        </Form.Field>
        <Form.Field>
          <label>Sprint</label>
          <Dropdown name="sprint" fluid search multiple selection options={sprintOptions} onChange={this.onInputChange} />
        </Form.Field>
      </Form>
    );
  }
}

export default TaskCreateUpdateView;
