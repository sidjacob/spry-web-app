import React from 'react';
import { Form, Dropdown } from 'semantic-ui-react';
import { createContributor } from '../../data/actions';
import Validator from '../../data/validator';
import CreateUpdateView from '../base/CreateUpdateView';
import { getContributorRoles, getContributorSkills } from '../../config/spryConfig';

class ProjectCreateUpdateView extends CreateUpdateView {

  constructor(props) {
    super(props);
  }

  getInitialState() {
    return { project: {} };
  }

  initializePropertiesForNewInstance() {
    // No properties to initialize
  }

  validate() {
    let validator = new Validator();

    return validator.getResult();
  }

  convertDataTypes() {
    // No datatypes to convert
  }

  dispatchCreateAction() {
  }
  
  getTitle() {
    return this.state.isCreate ? 'Add project' : 'Edit project'
  }

  renderForm() {
    return <div>Form to create a project goes here</div>;
  }
}

export default ProjectCreateUpdateView;
