import React from 'react';
import { Container, Header, Step } from 'semantic-ui-react';

import NavView from '../nav/NavView';
import SprintCreateUpdateView from '../sprints/SprintCreateUpdateView';
import ContributorCreateUpdateView from '../contributors/ContributorCreateUpdateView';
import ProjectCreateUpdateView from '../projects/ProjectCreateUpdateView';

class EmptyStateView extends React.Component {

  constructor(props) {
    super(props);
    this.handleCreateButtonClick = this.handleCreateButtonClick.bind(this);
  }

  handleCreateButtonClick() {
    this.createView.open();
  }

  render() {

    let createView = <SprintCreateUpdateView modal='false' store={this.props.store} spryIndex={this.props.spryIndex} 
        ref={el => { this.createView = el }} />;
  
    if (!this.props.spryIndex.hasContributors()) {
      createView = <ContributorCreateUpdateView modal='false' store={this.props.store} spryIndex={this.props.spryIndex} 
          ref={el => { this.createView = el }} />;
    } else if (!this.props.spryIndex.hasProjects()) {
      createView = <ProjectCreateUpdateView modal='false' store={this.props.store} spryIndex={this.props.spryIndex} 
          ref={el => { this.createView = el }} />;
    }
    
    let steps = [];
    steps.push({
      icon: 'user',
      completed: this.props.spryIndex.hasContributors(), 
      active: !this.props.spryIndex.hasContributors(),
      title: 'Add a contributor'
    });
    steps.push({
      icon: 'lab',
      completed: this.props.spryIndex.hasProjects(),
      active: this.props.spryIndex.hasContributors() && !this.props.spryIndex.hasProjects(),
      title: 'Add a project'
    });
    steps.push({
      icon: 'tasks',
      completed: this.props.spryIndex.hasSprints(), 
      active: this.props.spryIndex.hasProjects() && !this.props.spryIndex.hasSprints(),
      title: 'Create a sprint'
    });

    return (
      <div>
        <NavView showNavItems='false' />
        <Container className="empty-state-content" fluid textAlign='center'>
          <Header as='h2'>Complete these steps to get started</Header>
          <Step.Group items={steps}></Step.Group>
        </Container>
        <Container style={{paddingTop: '10px'}}>{createView}</Container>
      </div>
    );
  }
}

export default EmptyStateView;
