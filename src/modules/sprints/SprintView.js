import React from 'react';
import { Button, Dropdown } from 'semantic-ui-react';
import SprintCreateUpdateView from './SprintCreateUpdateView';

class PlanView extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      lastUpdated: new Date(),
      selectedSprintId: null
    };
    this.handleNewSprintButtonClick = this.handleNewSprintButtonClick.bind(this);
    this.handleSprintSelectionChange = this.handleSprintSelectionChange.bind(this);
    this.props.store.subscribe(() => this.handleStoreStateChange());
  }

  handleStoreStateChange() {
    if (this.props.store.getState().client.dataIndexed) {
      this.setState({ ...this.state, lastUpdated: new Date() });
    }
  }

  handleNewSprintButtonClick() {
    this.sprintCreateUpdateView.open();
  }

  handleSprintSelectionChange(event, data) {
    this.setState(...this.state, { selectedSprintId: data.value });
  }

  render() {

    const sprintOptions = this.props.spryIndex.sortedSprints.map(x => { return { text: x.name, value: x.id } });
    let selectedValue = sprintOptions[0].value;

    if (this.state.selectedSprintId) {
      selectedValue = this.state.selectedSprintId;
    }

    return <div>
             <div>
               <Dropdown onChange={this.handleSprintSelectionChange} 
                 value={selectedValue} selection options={sprintOptions} floated='left'/>
              <Button icon='plus' content='New sprint' color='blue' 
                 onClick={this.handleNewSprintButtonClick} />
             </div>
             <SprintCreateUpdateView store={this.props.store} spryIndex={this.props.spryIndex} 
                ref={el => { this.sprintCreateUpdateView = el }} />
           </div>;
  }
}

export default PlanView;
