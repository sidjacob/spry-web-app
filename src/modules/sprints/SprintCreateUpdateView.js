import React from 'react';
import { Form } from 'semantic-ui-react';
import yeast from 'yeast';
import Validator from '../../data/validator';
import { createSprint } from '../../data/actions';
import CreateUpdateView from '../base/CreateUpdateView';


class SprintCreateUpdateView extends CreateUpdateView {
    
  constructor(props) {
    super(props);
    this.onInputChange = this.onInputChange.bind(this);
  }
  
  onInputChange(event, data) {
    this.state.sprint[data.name] = data.value.trim();
  }
  
  getInitialState() {
    return {sprint: {}};
  }
  
  initializePropertiesForNewInstance() {
    
    let id = yeast();
    // Make sure the id is unique
    while(this.props.spryIndex.getSprint(id)) {
      id = yeast();
    }
    this.state.sprint.id = id;
  }
  
  validate() {
      let validator = new Validator();
      return validator
              .validateId('id', this.state.sprint.id)
              .validateName('name', this.state.sprint.name)
              .validateDate('startDate', this.state.sprint.startDate)
              .getResult();
  }
  
  convertDataTypes() {
    this.state.sprint.startDate = new Date(this.state.sprint.startDate);
  }
  
  dispatchCreateAction() {
    this.props.store.dispatch(createSprint(this.state.sprint));
  }
  
  getTitle() {
    return this.state.isCreate ? 'Create new sprint' : 'Edit sprint';
  }
  
  renderForm() {
    return <Form>
              <Form.Field>
                <label>Sprint Name *</label>
                <Form.Input error={this.state.validationResult.fields.name} name='name' 
                  onChange={this.onInputChange} placeholder='Minimum 2 characters' />
              </Form.Field>
              <Form.Field>
                <label>Start date *</label>
                <Form.Input error={this.state.validationResult.fields.startDate} name='startDate' 
                  onChange={this.onInputChange} placeholder='YYYY-MM-DD' />
              </Form.Field>
           </Form>;
  }
}

export default SprintCreateUpdateView;
